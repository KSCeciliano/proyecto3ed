import cv2
import numpy as np
import random


class Nodo:
  #Inicializacion de nodo
  def __init__(self, valor = None, color ="", forma =""):
    self.valor = valor 
    self.color =color
    self.forma = forma

class grafo:
  #Inicializacion de grafo
  def __init__(self):
    self.matriz = []

  #Agrega un nodo al grafo
  def agregar(self, color, forma):
    valor = len(self.matriz) + 1
    temp = Nodo(valor, color, forma)
    if(self.matriz == []):  #Si es el primer nodo
      self.matriz = [[temp,0]]
    else:
      self.matriz = self.matriz + [[temp]] #Añadir fila
      n = len(self.matriz) - 1
      a = len(self.matriz) - 1
      x = 0
      for x in range(n):  #Por cada nodo en la matriz
       tempNumero = random.randint(0,100) #Peso random para arista
       if (self.matriz[x][0].forma == self.matriz[a][0].forma or self.matriz[x][0].color == self.matriz[a][0].color): #Si se relaciona
          self.matriz[x] = self.matriz[x] + [tempNumero]
          self.matriz[a] = self.matriz[a] + [tempNumero]
       else:  #Si no se relaciona
          self.matriz[x] = self.matriz[x] + ["-"]
          self.matriz[a] = self.matriz[a] + ["-"]
      self.matriz[a] = self.matriz[a] + [0]
    del temp

  def imprimir_contenido(self):
    if self.matriz == []:
      print("No hay elementos")
    n = len(self.matriz)
    for x in range(n):
      m = len(self.matriz[x])
      for y in range(m):
        if (type(self.matriz[x][y]) != int and type(self.matriz[x][y]) != str):
          print("El nodo",self.matriz[x][y].valor,"su forma es un",self.matriz[x][y].forma,"y el color es",self.matriz[x][y].color,"y sus conexiones")
        else:
          if(self.matriz[x][y] != 0 and self.matriz[x][y] != "-"):
            print("\tPeso de ",self.matriz[x][y],"con el nodo", self.matriz[y-1][0].valor)

  def imprimir_nodo(self, valor):
    if self.matriz == []:
      print("No hay elementos")
    n = len(self.matriz)
    for x in range(n):  #Por cada nodo en matriz
      m = len(self.matriz[x])
      if(self.matriz[x][0].valor == valor):
        for y in range(m):  #Por cada columna
          if (type(self.matriz[x][y]) != int and type(self.matriz[x][y]) != str):
            print("\nEl nodo",self.matriz[x][y].valor,"su forma es un",self.matriz[x][y].forma,"y el color es",self.matriz[x][y].color, "y sus conexiones")
          else:
            if(self.matriz[x][y] != 0 and self.matriz[x][y] != "-"):
              print("\tPeso de ",self.matriz[x][y],"con el nodo", self.matriz[y-1][0].valor)

  def prim(self):
    matriz_temp = self.matriz
    return matriz_temp

  def Dijkstra(self, valor1, valor2, camino = "", peso = 0):
    if self.matriz == []:
      print("No hay elementos")
      return
    valor = 0
    menor_peso = 999
    n = len(self.matriz)
    for x in range(n):  #Por cada nodo en matriz
      m = len(self.matriz[x])
      if(self.matriz[x][0].valor == valor1): #Si es el nodo

        for y in range(m):  #Por cada columna
          valor = y+1
          if (type(self.matriz[x][y]) != int and type(self.matriz[x][y]) != str): #Si es un puntero a un nodo
            print("\nNodo ", self.matriz[x][y].valor)
          else:
            if(self.matriz[x][y] != 0 and self.matriz[x][y] != "-"): #Si hay Peso
              if(y+1 == valor2):  #Si el nodo1 tiene conexion con el nodo2(Final)
                valor = y+1
                menor_peso = self.matriz[x][y]
                camino = camino + " " + str(valor)
                peso += menor_peso
                print("\n\tResultado Final")
                print("Camino: ", camino)
                print("Peso: ", peso)
                return

              if(self.matriz[x][y]<menor_peso): #Si el peso de esta conexion es el menor hasta revisado ahora
                valor = y+1
                menor_peso = self.matriz[x][y]

                
                
        camino = camino + " " + str(valor)
        peso = peso + menor_peso
        print("Iteración")
        print("Camino: ", camino)
        print("Peso: ", peso)
        self.Dijkstra(valor, valor2, camino, peso) #Recursión
        break;

  def nodo_a_nodo(self, valor, valor2):
    self.matriz
    valor = valor - 1
    valor2 = valor2 - 1
    numuti = []
    primnodo = 0
    if self.matriz == []:
      print("No hay elementos")
    n = len(self.matriz)
    for x in range(n):
      if(self.matriz[x][0].valor == valor):
        primnodo = x
    print("El nodo inicial es:",self.matriz[primnodo][0].valor)
    print("El recorrido es:")
    while(self.matriz[primnodo][0].valor != valor2):
      esta = 0
      rec = 0
      recnum = 110
      m = len(self.matriz[primnodo])
      for y in range(m):
        if (y == 0):
          y = 0
        elif(self.matriz[primnodo][y] != 0 and self.matriz[primnodo][y] != "-"):
          if(recnum > self.matriz[primnodo][y]):
            if(numuti == []):
              numuti = numuti + [self.matriz[primnodo][y]]
              recnum = self.matriz[primnodo][y]
              temp = y - 1
            else:
              h = len(numuti)
              for o in range(h):
                if(self.matriz[primnodo][y] == numuti[o]):
                  esta = 1
              if(esta == 0):
                numuti = numuti + [self.matriz[primnodo][y]]
                recnum = self.matriz[primnodo][y]
                temp = y - 1
      if(recnum == 110):
        self.matriz[primnodo][0].valor = valor2
        print("No hay camino")
      else:
        primnodo = temp
        print(self.matriz[primnodo][0].valor)



miGrafo = grafo() #Inicializacion de grafo en variable

font = cv2.FONT_HERSHEY_COMPLEX

img = cv2.imread("Grafo.jpg") #Se carga la imagen en la variable
imgGrey = cv2.imread("Grafo.jpg", cv2.IMREAD_GRAYSCALE) #Imagen en escala de grises
_, thrash = cv2.threshold(imgGrey, 240, 255, cv2.THRESH_BINARY)
contours, _ = cv2.findContours(thrash, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE) #Figuras en la imagen

print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
print("\t- Leyendo datos de imagen -")

for cnt in contours:  #Por cada figura
    approx = cv2.approxPolyDP(cnt, 0.01* cv2.arcLength(cnt, True), True)
    cv2.drawContours(img, [approx], 0, (0, 0, 0), 5)
    x = approx.ravel()[0]
    y = approx.ravel()[1] - 5
    if len(approx) == 3:  #Si tiene 3 lineas es un Triangulo
        miGrafo.agregar(random.choice(["azul", "verde", "amarillo", "rojo"]),"triangulo")
        print("Triangulo")
        cv2.putText(img, "Triangle", (x, y), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0)) #Se le pone texto a la figura en la imagen

    elif len(approx) == 4: #Si tiene 4 lineas es un Cuadrado o un Rectangulo
        x1 ,y1, w, h = cv2.boundingRect(approx)
        aspectRatio = float(w)/h
        if aspectRatio >= 0.95 and aspectRatio <= 1.05:
          miGrafo.agregar(random.choice(["azul", "verde", "amarillo", "rojo"]),"cuadrado") #Se agrega nodo a grafo
          print("Cuadrado")
          cv2.putText(img, "square", (x, y), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0)) #Se le pone texto a la figura en la imagen
        else:
          miGrafo.agregar(random.choice(["azul", "verde", "amarillo", "rojo"]),"rectangulo") #Se agrega nodo a grafo
          print("Rectangulo")
          cv2.putText(img, "rectangle", (x, y), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0)) #Se le pone texto a la figura en la imagen

    elif len(approx) == 5:  #Si tiene 5 lineas en un Pentagono
        miGrafo.agregar(random.choice(["azul", "verde", "amarillo", "rojo"]),"pentagono") #Se agrega nodo a grafo
        print("Pentagono")
        cv2.putText(img, "Pentagon", (x, y), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0)) #Se le pone texto a la figura en la imagen

    else: #Cualquier otra se toma como Circulo
        miGrafo.agregar(random.choice(["azul", "verde", "amarillo", "rojo"]),"circulo")
        print("Circulo")
        cv2.putText(img, "Circle", (x, y), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0))

print("\t- Datos cargados desde imagen correctamente -\n\n")

print("\t- Contenido del Grafo -\n")
miGrafo.imprimir_contenido()
print("\t- Fin del Contenido del Grafo -\n\n")

print("\n\t- Imprimir Nodo -\n")
print("\t- Nodo 2 -")
miGrafo.imprimir_nodo(2)

print("\n\n\t- Nodo 20 -")
miGrafo.imprimir_nodo(20)


print("\n\n\nRuta más corta entre dos nodos.")
#miGrafo.prim()
print("\n\t- Dijkstra -")
miGrafo.Dijkstra(2,20)

print("\n\n\t- Nodo a Nodo -")
miGrafo.nodo_a_nodo(2,20)


"""
cv2.imshow("Grafo", img)
cv2.waitKey(0)
cv2.destroyAllWindows()
"""